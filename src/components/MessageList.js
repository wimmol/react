import React from 'react'
import Message from "./Message";

class MessageList extends React.Component {

    render() {
        return (
            <div className={'message-list'}>
                {this.props.messages.map(message => {
                    return <Message message={message} key={message.id} editMessage={this.props.editMessage} deleteMessage={this.props.deleteMessage}/>
                })}
            </div>
        )
    }

}

export default MessageList;