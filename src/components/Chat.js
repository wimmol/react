import React, {Suspense} from 'react'
import Header from "./Header";
import MessageList from "./MessageList";
import Input from "./Input";

async function getData() {
    let res = null;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://edikdolynskyi.github.io/react_sources/messages.json', false);
    xhr.send();
    if (xhr.status === 200) {
        res = JSON.parse(xhr.responseText);
    }
    return res;
}

function getInfo(messageArr) {
    let data = {
        messages: messageArr.sort((user1, user2) => user1.createdAt > user2.createdAt ? 1 : -1),
        headerData: {
            chatName: 'My Chat',
            usersNum: messageArr.map(user => user.userId).filter((id, index, arr) => arr.indexOf(id) === index).length,
            messagesNum: messageArr.length
        }
    };
    data.headerData.lastMessageDate = data.messages[data.messages.length - 1].createdAt;
    return data;
}

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
                headerData: {
                    chatName: 'My Chat',
                    usersNum: 0,
                    messagesNum: 0,
                    lastMessageDate: 0
            }
        };
        this.addMessage = this.addMessage.bind(this);
        this.editMessage = this.editMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);

    }

    async componentDidMount() {
        const data = await getData();
        this.setState(getInfo(data))
    }

    addMessage(message) {
        let newItem = this.state.messages;
        newItem.push(message);
        this.setState(getInfo(newItem));
    }

    editMessage(message) {
        this.deleteMessage(message);
        let newItem = this.state.messages;
        newItem.push(message);
        this.setState(getInfo(newItem));
    }

    deleteMessage(message) {
        let newItem = this.state.messages;
        newItem.splice(this.state.messages.findIndex(_message => _message.id === message.id), 1);
        this.setState(getInfo(newItem));
    }

    render() {
        const data = this.state;
        return (
            <Suspense fallback={<h1>Loading...</h1>}>
                <div className={'chat'} >
                    <Header data={data.headerData}  />
                    <MessageList messages={data.messages} editMessage={this.editMessage} deleteMessage={this.deleteMessage}/>
                    <Input addMessage={this.addMessage}/>
                </div>
            </Suspense>
        )
    }

}

export default Chat;