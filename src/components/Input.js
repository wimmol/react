import React from 'react'

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <div className={'input'}>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type={'text'}
                        className={'new-message'}
                        onChange={this.handleChange}
                        value={this.state.text}
                    />
                    <button className={'send'}>
                        Send
                    </button>
                </form>
                <p>Edit - double click <span role={'img'} aria-label={'blabla'}>&#9996;</span>️</p>
            </div>
        );
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const newItem = {
            text: this.state.text,
            id: (+new Date()).toString(16),
            userId: 0,
            user: 'Rick',
            avatar: 'https://raskrasil.com/wp-content/uploads/Raskrasil-Rick-and-Morty-51.jpg',
            createdAt: Date(),
            editedAt: '',
            isYour: true
        };
        this.props.addMessage(newItem);
        this.setState(state => ({text: ''}));
    }
}

export default Input;