import React from 'react'

class Message extends React.Component {

    constructor(props) {
        super(props);
        this.state =  {
            text: this.props.message.text,
            editor: false,
            isLiked: false
        };
        this.doubleClick = this.doubleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.like = this.like.bind(this);
    }

    doubleClick() {
        if (this.props.message.isYour) {
            if (!this.state.editor) {
                this.setState({
                    text: this.props.message.text,
                    editor: true
                })
            } else {
                this.setState({
                    text: this.props.message.text,
                    editor: false
                })
            }
        }
    }

    deleteMessage() {
        this.props.deleteMessage(this.props.message);
    }

    handleChange(e) {
        this.setState({
            text: e.target.value,
            editor: true
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const editItem = {
            ...this.props.message,
            editedAt: Date(),
            text: this.state.text
        };
        this.props.editMessage(editItem);
        this.setState(state => ({
            editor: false
        }));
    }

    like() {
        this.setState({isLiked: !this.state.isLiked})
    }

    render() {
        const data = this.props.message;

        return (
            <div>
                <div className={`message-wrapper${data.isYour ? ' your' : ''}`}>
                    <img className={'avatar'} src={data.avatar} alt={'avatar'}/>

                    <div className={`message`} onDoubleClick={this.doubleClick}>
                        <p className={'username'}>{data.user}</p>
                        <p className={'message-text'}>{data.text}</p>
                        <p className={'message-date'}>{data.createdAt}</p>
                    </div>
                    <div className={`like${this.state.isLiked ? ' red' : ''}${data.isYour ? ' display-none' : ''}`} onClick={this.like}>
                    </div>
                </div>
                <div className={`editor-wrapper${!this.state.editor ? ' display-none' : ''}`}>
                    <form onSubmit={this.handleSubmit}>
                        <input
                            type={'text'}
                            className={'edit-message'}
                            onChange={this.handleChange}
                            value={this.state.text}
                        />
                        <button className={'edit'}>
                            Edit
                        </button>
                        <div className={'delete edit'} onClick={this.deleteMessage}>
                            Delete
                        </div>
                    </form>
                </div>
            </div>
        )
    }

}

export default Message;