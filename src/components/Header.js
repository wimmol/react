import React from 'react'

class Header extends React.Component {

    render() {
        return (
            <div className={'header'}>
                <p className={'chat-name'}>{this.props.data.chatName}</p>
                <p>{this.props.data.usersNum} users</p>
                <p>{this.props.data.messagesNum} messages</p>
                <div className={'last-message-date'}>
                  <p>Last message sent at {this.props.data.lastMessageDate} </p>
                </div>
            </div>
        )
    }

}

export default Header;